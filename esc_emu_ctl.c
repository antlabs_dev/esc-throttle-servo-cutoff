/**
 * @file 	esc_emu_ctl.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Main module
 */

#include <htc.h>
#include <stdint.h>

/**
 * Configuration bits
 */
__CONFIG(WDTE_ON & FOSC_INTOSCCLK & CP_OFF & PWRTE_ON & BOREN_OFF & MCLRE_OFF & LVP_OFF & CPD_OFF);
__IDLOC(DEAD);

/**
 * Device configuration stored in EEPROM
 */
#define COUNTDOWN_MINS 5
#define COUNTDOWN_SECS 2
#define TIMER0_PRELOAD_CALIBRATION 25
#define MINIMUM_PWM_DUTY 13
#define MAXIMUM_PWM_DUTY 28
/**
 * 0x00 - countdown value minutes
 * 0x01 - countdown value seconds
 * 0x02 - TMR0 preload value (for calibration)
 * 0x03 - Min PWM duty (13 is about 0.87 ms)
 * 0x04 - Max duty (28 is about 1.88 ms)
 */
__EEPROM_DATA(COUNTDOWN_MINS, COUNTDOWN_SECS, TIMER0_PRELOAD_CALIBRATION, MINIMUM_PWM_DUTY, MAXIMUM_PWM_DUTY, 0xFF, 0xFF, 0xFF);

/**
 * Global state and configuration struct
 */
static volatile struct svs_cfg {
    uint16_t countdown_value; //<! Value in seconds to countdown (read from eeprom)
    uint16_t counter_seconds; //<! Global seconds counter
    uint8_t tmr0_preload;     //<! Preload this value to TMR0 used as 1 sec timer  (read from eeprom)
    uint8_t timeout_flag;     //<! Global flag indicates timeout
    uint8_t min_duty;         //<! PWM preload for minimum throttle (read from eeprom)
    uint8_t max_duty;         //<! PWM preload for maximum throttle (read from eeprom)
} cfg = {0, 0, 0, 0, 0, 0};

/**
 * Initialize all hardware
 */
static void init(void);

/**
 * Set position of servo (esc)
 * @param position - normally 13 = 872mkS duty, 28 = 1.88mS duty
 */
static void pwm_servo_set(uint8_t position);

/**
 * Set countdown state
 * @param state - 0 resets timer, 1 starts it with COUNTDOWN_VALUE seconds
 */
static void timeout_counter_set(uint8_t state);

/**
 * Entry point
 */
void main(void)
{
    init();

    uint8_t button_hold = 0;
    uint8_t smooth_duty = cfg.min_duty;
    uint8_t smooth_duty_up_flag = 0;
    uint8_t smooth_duty_iteration = 0;

    timeout_counter_set(0);
    pwm_servo_set(cfg.min_duty);

    uint8_t toggle_button_mode = RB0 == 0 ? 0 : 1u;
    if(!toggle_button_mode)
    {   // start at full speed on power on
        // after short delay
        volatile uint16_t i = 0;
        for(i = 0; i < 1000u; i++)
        {
            CLRWDT();
        }
        smooth_duty_up_flag = 1u;
    }

    while(1)
    {
        CLRWDT();

        if(toggle_button_mode)
        {   // Poll button
            if(RB0 == 0)
            {   // button pressed
                if(++button_hold > 75u)
                {
                    if(T0IE)
                    {
                        pwm_servo_set(cfg.min_duty);
                        timeout_counter_set(0);
                        // Lock device for a short period
                        volatile uint16_t i = 0;
                        for(i = 0; i < 1000u; i++)
                        {
                            CLRWDT();
                        }
                    }
                    else
                    {
                        smooth_duty_up_flag = 1u;
                    }
                    button_hold = 0;
                }
            }
            else
            {
                button_hold = 0;
            }
        }

        // In case of set to max duty do it smoothly
        if(smooth_duty_up_flag)
        {
            if(smooth_duty_iteration++ >= 25u)
            {
                smooth_duty_iteration = 0;
                pwm_servo_set(++smooth_duty);
                if(smooth_duty >= cfg.max_duty)
                {
                    timeout_counter_set(1u);
                    smooth_duty_up_flag = 0;
                    smooth_duty = cfg.min_duty;
                }
            }
        }

        // Check timeout flag
        if(cfg.timeout_flag)
        {
            cfg.timeout_flag = 0;
            pwm_servo_set(cfg.min_duty);
            timeout_counter_set(0);
        }
    }
}

static void init(void)
{
    // Enable low-frequency oscillator
    // Has to be 48KHz, but in my case it was 59.2KHz
    OSCF = 0;

    //Just for sure - clear ports and disable interrupts
    INTCON = 0;
    PORTB = 0;
    PORTA = 0;
    CCP1CON = 0;

    // Timer 0 enable with 1:64 prescale
    PS0 = 1u;
    PS1 = 0;
    PS2 = 1u;
    PSA = 0;
    T0CS = 0;

    // PORTB0 - input with pull-up from the button
    TRISB0 = 1u;
    nRBPU = 0;

    // PORTB3 - CCP output
    TRISB3 = 0;
    PR2 = 0b01001001;
    T2CON = 0b00000101;

    // Read configuration
    cfg.countdown_value = eeprom_read(0x00) * 60u;
    cfg.countdown_value += eeprom_read(0x01);
    cfg.tmr0_preload = eeprom_read(0x02);
    cfg.min_duty = eeprom_read(0x03);
    cfg.max_duty = eeprom_read(0x04);

    //Global enable interrupts
    GIE = 1u;
}

static void pwm_servo_set(uint8_t position)
{
    CCPR1L = position >> 2;
    CCP1CON = 0b00001100;
    CCP1CON |= (position & 0x3) << 4;
}

static void timeout_counter_set(uint8_t state)
{
    state &= 1u;
    if(state == 1u)
    {
        TMR0 = cfg.tmr0_preload;
        T0IE = 1u;
    }
    else
    {
        T0IE = 0;
        cfg.counter_seconds = 0;
    }
}

void interrupt isr(void)
{
    if(T0IF)
    {   // TMR0 interrupt
        T0IF = 0;
        TMR0 = cfg.tmr0_preload;
        if(++cfg.counter_seconds >= cfg.countdown_value)
        {
            cfg.timeout_flag = 1u;
        }
    }
}
