PICC=/mnt/video/hitech/picc-16/bin/picc
CHIP=16F628A
SOURCES=esc_emu_ctl.c
HEX=esc_emu_ctl.hex

all: 
	$(PICC) --CHIP=$(CHIP) $(SOURCES)

clean: 
	rm -fr *.pro *.as *.lst *.obj *.rlf *.cof *.hex *.hxl *.lst *.p1 *.pre *.sdb *.sym funclist
	@echo Done!

burn:
	op -d $(CHIP) -w ./$(HEX) -ee

burnpgm:
	picpgm -p ./$(HEX) 

asm:
	$(PICC) --CHIP=$(CHIP) $(SOURCES) -S

